module "vector"
vector = {}
---new
---@param x number
---@param y number
---@param z number
---@return vector
function vector.new(x,y,z) end
function vector.x() end
function vector.y() end
function vector.z() end
function vector:add() end
function vector:sub() end
function vector:mul() end
function vector:dot() end
function vector:cross() end
function vector:length() end
function vector:normalize() end
function vector:round() end
function vector:tostring() end