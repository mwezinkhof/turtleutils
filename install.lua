-- This utility downloads the installer
-- which in turn download/installs the rest of the program(s)

basePath = "https://bitbucket.org/mwezinkhof/turtleutils/raw/master/";
function pr(toprint)
	print("[kaazinator] " .. toprint)
end

function download(file, target)
	local response = http.get(basePath .. file)
	if response then
		local content = response.readAll()
		local f = fs.open(target, "w")
		f.write(content)
		f.close()
		response.close()
		return true
	else
		pr("cant find file")
		pr(basePath .. file)
		return false
	end
end
pr("Downloading new installer")
download("lib/installProcess.lua", "lib/installProcess")

if fs.exists("lib/installProcess") then
	pr("Download success")
	pr("Running the installer")
	shell.run("lib/installProcess")
else
	pr("Couldn't download the file [is the http api on?]")
end