-- usage: 
-- local vec = Vector.new([x,y,z,direction]) -- optional params
-- 
local function accessor(obj, name, fieldIndex, mod)
	
	obj['get' .. name] = function(self)
		return self[fieldIndex]
	end

	obj['set' .. name] = function(self, v)
		self[fieldIndex] = v
	end

	if mod then
		obj['modify' .. name] = function(self, v)
			self[fieldIndex] = self[fieldIndex] + v
		end
	end
end

Vector = {0, 0, 0, 2}

function Vector:new(...) 
	local obj = {} 
	self.__index = self 
	setmetatable(obj, self) 
	if obj.__construct then
	    return obj:__construct(...) 
	end
	return obj
end

function Vector:__construct(x, y, z,direction)

	self[1], self[2], self[3] ,self[4] = x, y, z, direction
	return self
end

-- Getters & Setters  getX, setX, modifyX
accessor(Vector, 'X', 1, true)
accessor(Vector, 'Y', 2, true)
accessor(Vector, 'Z', 3, true)
accessor(Vector, 'Direction', 4, true)
function Vector:toArray()
	return {self[1],self[2],self[3],self[4]}
end
function Vector:loadFromArray(arr)
	self[1] = arr[1]
	self[2] = arr[2]
	self[3] = arr[3]
	self[4] = arr[4]
end
function Vector:modifyDirection(directionMod)
	self[4] = (4 + self[4] + directionMod) % 4
end
function Vector:moveInDirection(amount)
	if amount == nil then amount = 1 end
	for i=1,amount,1 do
			if self[4] == 0 then self:modifyZ(1) end
			if self[4] == 1 then self:modifyX(1) end
			if self[4] == 2 then self:modifyZ(-1) end
			if self[4] == 3 then self:modifyX(-1) end
	end
end
function Vector:__tostring()
	return string.format("Vector(%d, %d, %d, %d)", self:getX(), self:getY(), self:getZ(),self:getDirection())
end

function Vector:calcWalkDistance() --manhattan distance
	return math.abs(self[1] - vec[1]) + math.abs(self[2] - vec[2]) + math.abs(self[3] - vec[3])
end

function Vector:blocksAway(vec)
	return math.max(math.abs(self[1] - vec[1]),math.abs(self[2] - vec[2]),math.abs(self[3] - vec[3]))
end

function Vector:calcDistance(vec)
	return math.sqrt(
		(self[1] - vec[1])^2 +
		(self[2] - vec[2])^2 +
		(self[3] - vec[3])^2
	)
end
