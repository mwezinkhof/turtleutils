require('lib.faketurtle')
local t = require('lib.kurtle')

t.load()


while true do
    local event, key = os.pullEvent("key")
    if key == keys.up then
        if kurtle.move() then
            print("Moved to: ", kurtle.position:tostring())
        end
    elseif key == keys.left then
        kurtle.turnLeft()
        print("Turned  left. Direction: ", kurtle:getDirectionName())
    elseif key == keys.right then
        kurtle.turnRight()
        print("Turned right. Direction: ", kurtle:getDirectionName())
    else
        break
    end
end