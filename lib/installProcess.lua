-- This utility installs or updates all the required scripts from the repository :)
basePath = "https://bitbucket.org/mwezinkhof/turtleutils/raw/master/";

function download(file, target)
	local response = http.get(basePath .. file)
	if response then
		local content = response.readAll()
		local f = fs.open(target, "w")
		f.write(content)
		f.close()
		response.close()
	else
		error("Can't find the file ".. file .." :(")
	end

end

download("to_download_list.txt", "data/to_download_list.txt")
print("Finnished downloading the file list")
fileList = fs.open("data/to_download_list.txt","r")
local fileEntry = fileList.readLine()
while fileEntry do
	print("downloading ".. fileEntry)
	download(fileEntry .. ".lua", fileEntry .. ".lua")
	fileEntry = fileList.readLine()
end
fileList.close()
print("Done!")
