-- basic persistence config
-- for now it only supports 1 dimensional tables [config.abc] and not [config.abc.de]
-- example usage:
--config = configFile:new("test", {shouldRun=false})
--if config.shouldRun ~= true then
--    return
--end
--config.runcount = (config.runcount or 0) + 1

configFile = {
}
local function save (configData, filePath)
    local file = fs.open(filePath, "w")
    print("saving")
    print(textutils.serialize(configData))
    file.write(textutils.serialize(configData))
    file.close()
end

---new
---@param filePath string path to file
---@param baseConfig table basic config
---@return table config table loaded with file
function configFile:new(filePath, baseConfig, parent)
    if type(baseConfig) == "table" then
        ret = baseConfig
    else
        ret = {}
    end

    if fs.exists(filePath) then
        local file = fs.open(filePath, "r")
        local content = textutils.unserialize(file.readAll());
        if type(content) == 'table' then
            for i, v in pairs(content) do
                print(i)
                ret[i] = v;
            end
        end
        file.close()
    end
    -- keep a private access to original table
    local _original = ret
    ret = {
        parent=function(t)
        end
    }
    local proxyTable = {
        __index = function(t, k)
            return _original[k]   -- access the original table
        end,
        save = save,
        __newindex = function(t, k, v)
            local isDifferent = true
            if _original[k] ~= nil then
                isDifferent = _original[k] ~= k
            end
            _original[k] = v
            if isDifferent then
                save(_original, filePath)
            end
        end,
    }
    setmetatable(ret, proxyTable)
    return ret
end