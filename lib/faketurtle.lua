if not turtle then
    --for testing purposes
    turtle = {}
    turtle.forward = function()
        return true
    end
    turtle.turnLeft = function()
        return true
    end
    turtle.turnRight = function()
        return true
    end
    function turtle.back() end
    function turtle.up() end
    function turtle.down() end
    function turtle.select() end
    function turtle.getItemCount() end
    function turtle.getItemSpace() end
    function turtle.dig() end
    function turtle.digUp() end
    function turtle.digDown() end
    function turtle.place() end
    function turtle.placeUp() end
    function turtle.placeDown() end
    function turtle.attack() end
    function turtle.attackUp() end
    function turtle.attackDown() end
    function turtle.detect() end
    function turtle.detectUp() end
    function turtle.detectDown() end
    function turtle.compare() end
    function turtle.compareUp() end
    function turtle.compareDown() end
    function turtle.compareTo() end
    function turtle.inspect() end
    function turtle.inspectUp() end
    function turtle.inspectDown() end
    function turtle.transferTo() end
    function turtle.drop() end
    function turtle.getItemDetails() end
    function turtle.dropUp() end
    function turtle.dropDown() end
    function turtle.suck() end
    function turtle.suckUp() end
    function turtle.suckDown() end
    function turtle.getFuelLevel() end
    function turtle.refuel() end
    function turtle.craft()  end
end