--cooldown between actions
local gcd = 0.5

--DIG
function digforward()
	while turtle.detect() do
		turtle.dig()
		sleep(gcd)
	end
end

function digUp() --same story
while turtle.detectUp() do
	turtle.digUp()
	sleep(gcd)
end
end

function digDown()
	if turtle.detectDown() then
		turtle.digDown()
	end
end

local function _f()
	digforward()
	turtle.forward()
end

local function _d()
	digDown()
	turtle.down()
end

local function _u()
	digUp()
	turtle.up()
end

-- FORWARDS
function f(amount)
	if amount == nil or amount < 1 then
		amount = 1
	end
	for i = 1, amount do
		_f()
	end
end

-- up, up and away
function up(amount)
	if amount == nil or amount < 1 then
		amount = 1
	end
	for i = 1, amount do
		_u()
	end
end

-- down we go
function down(amount)
	if amount == nil or amount < 1 then
		amount = 1
	end
	for i = 1, amount do
		_d()
	end
end

function left()
	turtle.turnLeft()
end

function right()
	turtle.turnRight()
end

function dropItems(slot)
	if slot ~= nil and slot > 0 and slot < 17 then
		turtle.select(slot)
	end
	turtle.drop()
end
function dropItemsDown(slot)
	if slot ~= nil and slot > 0 and slot < 17 then
		turtle.select(slot)
	end
	turtle.dropDown()
end
function getItemCount(slot)
	return turtle.getItemCount(slot)
end

function isItemslotA(itemSlot, itemName, itemDamage)
	turtle.select(itemSlot)
	local data = turtle.getItemDetail()
	if data then
		if data.name == itemName then
			if itemDamage ~= nil and itemDamage > 0 then
				if data.damage then
					return itemDamage == data.damage
				end
				return false
			end
			return true
		end
	end
	return false
end

function dropAll()
	for i = 1, 16 do
		dropItems(i)
	end
end

function dropItemRange(from, to)
	for i = from, to do
		if turtle.getItemCount(i) then
			dropItems(i)
		end
	end
end
function dropItemDownRange(from, to)
	for i = from, to do
		if turtle.getItemCount(i) then
			dropItemsDown(i)
		end
	end
end

function sortInventory()
	for currentSlot = 1, 16 do
		local itemCount = turtle.getItemCount(currentSlot)
		if itemCount and itemCount > 0 then
			turtle.select(currentSlot)
			for targetSlot = currentSlot, 16 do
				if turtle.compareTo(targetSlot) then
					turtle.select(targetSlot)
					turtle.transferTo(currentSlot)
				end
			end
		end
	end
end

return class