local defaultPosition = vector.new(0, 0, 0)
local defaultPdirection = vector.new(0, 0, 1) -- start in de richting van positieve z-as

local scriptVersion = 1

local locationFile = "data/position.txt"

local position = vector.new(0, 0, 0)
local direction = vector.new(0, 0, 1) -- start in de richting van positieve z-as
if kurtle and kurtle.version and kurtle.version == scriptVersion then
    print('already loaded, use cache')
    return kurtle
end

kurtle = {
    version= scriptVersion,
    position=position,
    direction=direction,
}

function kurtle:savePosition()
    local file = fs.open(locationFile, "w")
    file.write(kurtle.position:tostring() .. "\n")
    file.write(kurtle.direction:tostring() .. "\n")
    file.close()
end

local function split_with_comma(str)
    local fields = {}
    for field in str:gmatch('([^,]+)') do
        fields[#fields + 1] = field
    end
    return fields
end

function kurtle:loadPosition()
    if fs.exists(locationFile) then
        if fs.getSize(locationFile) >= 2 then
            local file = fs.open(locationFile, "r")
            local line1 = split_with_comma(file.readLine())
            local line2 = split_with_comma(file.readLine())
            kurtle.position = vector.new(line1[1], line1[2], line1[3])
            kurtle.direction = vector.new(line2[1], line2[2], line2[3])
            file.close()
        end
        print("load location: " .. kurtle.position:tostring())
        print("load direction: " .. kurtle.direction:tostring())
    end
end

function  kurtle:getDirectionName()
    if kurtle.direction.z == 1 then
        return 'north'
    end
    if kurtle.direction.z == -1 then
        return 'south'
    end
    if (kurtle.direction.x == 1) then
        return 'east'
    end
    if (kurtle.direction.x == -1) then
        return 'west'
    end
end

function kurtle:move()
    if turtle.forward() then
        print('old pos " .. ' .. kurtle.position:tostring() .. ' - rotation: ' .. kurtle.direction:tostring())
        kurtle.position = kurtle.position + kurtle.direction
        print('moving to: ' .. kurtle.position:tostring())
        kurtle.savePosition()
        return true
    end
    return false
end

function  kurtle.turnLeft()
    if turtle.turnLeft() then
        kurtle.direction = vector.new(-kurtle.direction.z, kurtle.direction.y, kurtle.direction.x)
        kurtle.savePosition()
    end
end

function  kurtle.turnRight()
    if turtle.turnRight() then
        kurtle.direction = vector.new(kurtle.direction.z, kurtle.direction.y, -kurtle.direction.x)
        kurtle.savePosition()
    end
end
function kurtle.load()
    kurtle.loadPosition()
    print("loc: " .. kurtle.position:tostring())
    print("facing: " ..kurtle.getDirectionName())
    print("boop up complete!")
end

return kurtle