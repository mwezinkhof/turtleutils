local args = { ... }
if #args ~= 1 then
	term.clear()
	print("Usage: woodcutter <trees>")
	print("Remember to put a chest below the startpoint")
	print("Also, a [birch] sappling in the first slot!")
	print("And the last slot is for fuel")
	return
end
local shouldRun = true
local minFuel = 200
local targetFuelLevel = 1000
local saplingSlot = 1
local fuelSlot = 16
local woodSlot = 2
local treeCount = 10

kurtle = require('.\\lib.kurtle')
kurtle.load()

function prepare()
	if kurtle.isItemslotA(saplingSlot, "minecraft:sapling") == false then
		print("No sappling found in slot " .. saplingSlot)
		return false
	elseif kurtle.getItemCount(saplingSlot) <= treeCount then
		print("Not enough saplings :( [".. treeCount .."]")
		return false
	end

	return true
end

-- Called when in front of a chest/container
function refuel()
	turtle.select(fuelSlot)
	local itemCount = turtle.getItemCount(fuelSlot)
	turtle.suck(64 - itemCount)
	if turtle.getFuelLevel() < targetFuelLevel then
		if turtle.getItemCount(fuelSlot) > 1 then
			turtle.refuel(turtle.getItemCount(fuelSlot) - 1)
		end
	end
end

function blockInfrontIsA(blockname)
	local success, itemData = turtle.inspect()
	if success and itemData then
		return blockname == itemData.name
	end
	return false
end

function blockAboveIsA(blockname)
	local success, itemData = turtle.inspectUp()
	if success and itemData then
		return blockname == itemData.name
	end
	return false
end

function chopTree()
	local success, itemData = turtle.inspect()
	local ups = 0
	if success and itemData then
		if itemData.name == "minecraft:log" then
			kt.f()
			while blockAboveIsA("minecraft:log") do
				kt.up()
				ups = ups + 1
			end
			kt.down(ups)
			turtle.back()
		end
	end
	turtle.select(saplingSlot)
	if turtle.getItemCount(saplingSlot) > 1 then
		turtle.place()
	end
end

while shouldRun and prepare() do
	-- LOGIC STARTS HERE
	kt.sortInventory()
	kt.dropItemDownRange(2, 15)
	if minFuel > turtle.getFuelLevel() then
		kt.right()
		refuel()
		if minFuel > turtle.getFuelLevel() then
			print("Need more fuel! I'm waiting :)")
			while minFuel > turtle.getFuelLevel() do
				refuel()
				os.sleep(10)
			end
		end
		kt.left()
	end
	kt.f(3)
	for i = 1, treeCount do
		kt.left()
		chopTree();
		kt.right()
		kt.f()
	end
	kt.right()
	kt.right()
	for i = 1, treeCount do
		turtle.suck()
		kt.left()
		chopTree()
		kt.right()
		kt.f()
	end
	kt.f(3)
	kt.right()
	kt.right()
	print("Trying again in 3 minutes")
	os.sleep(180)
end