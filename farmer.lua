local args = { ... }
if #args ~= 2 then
	print ("Usage: farmer <width> <height>")
	print ("Width = forward , height = right")
	print ("Remember to put a chest below the startpoint")
	print ("'farmer 3 3' example")
	print ("x x x")
	print ("x x x")
	print ("x x x")
	print ("t")
	print ("--x-->")
	return
end

local curdir = 1
local up = 1
local right = 2
local down = 3
local left = 4
local direction = {{0,1},{1,0},{0,-1},{-1,0}}

local relPos = {0,0}

local farmX = tonumber(args[1])
local farmY = tonumber(args[2])

local function plantAndHarvest()
	turtle.digDown()
    turtle.placeDown()
end

local function move(amount)
	for i=1,amount do
		turtle.move()
	end
end
local function left()
	direction = (3 + direction) % 4
	turtle.turnLeft()
end
local function right()
	direction = (1+direction)%4
	turtle.turnRight()
end
local function goToRelative()

end
local function goHome()

end 

local function fullIteration()
	local b = farmX
	if farmX%2==1 then
		b = b + 1
	end
	move()
	plantAndHarvest()
	for i = 1, farmX do
		for j = 1, farmY do
			plantAndHarvest()
			move()
		end
		plantAndHarvest()
		if(farm%2==1) then
			right()
			move()
			right()
		elseif i < farmX then
			left()
			move()
			left()
		end
	end
	if farmX%2 == 1 then
		move(farmX)
	end
	right()
	move(b)
	right()
end

fullIteration()