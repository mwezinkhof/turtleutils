os.loadAPI("lib/kt")

--yes yes feel free to change
local quaryX = 16
local quaryY = 16

--GTFO
local itemBlackList = {}
local relPos = { x = 0, y = 0, z = 0 }
local HOME = { x = 0, y = 0, z = 0 }
local lastminedPos = { x = 0, y = 0, z = 0 }
local directionMod = {}
directionMod[1] = { x = 0, y = 0, z = 1 } -- South
directionMod[2] = { x = 1, y = 0, z = 0 } -- East
directionMod[3] = { x = 0, y = 0, z = -1 } -- North
directionMod[4] = { x = -1, y = 0, z = 0 } -- West
directionMod[5] = { x = 0, y = 1, z = 0 } -- Up
directionMod[6] = { x = 0, y = -1, z = 0 } -- Down

local SOUTH, EAST, NORTH, WEST, UP, DOWN = 1, 2, 3, 4, 5, 6
local curDir = 1
local blackListFile = "data/quarry/blacklist"
local currentLocationFile = "data/quarry/location"
local lastMinedPosition = "data/quarry/lastmined"
local function saveLocation()
	local fh = fs.open(currentLocationFile, "w")
	fh.write(textutils.serialize({ relPos.x, relPos.y, relPos.z, curDir }))
	fh.close()
end

local function modCoordinates(posVector)
	relPos.x = relPos.x + posVector.x
	relPos.y = relPos.y + posVector.y
	relPos.z = relPos.z + posVector.z
	saveLocation()
end

local function right()
	if turtle.turnRight() then
		curDir = curDir - 1
		if curDir == 0 then
			curDir = 4
		end
		saveLocation()
	end
end

local function left()
	if turtle.turnLeft() then
		curDIr = curDir + 1
		if curDir == 5 then
			curDir = 1
		end
		saveLocation()
	end
end

local function turnToFace(resultDirection)
	local targetDirection = resultDirection
	local leftCost = 0
	local rightCost = 0
	local currentDirection = curDir - 1
	targetDirection = targetDirection - 1 % 4

	if targetDirection > currentDirection then
		leftCost = 4 + currentDirection - targetDirection
		rightCost = targetDirection - currentDirection
	else
		leftCost = currentDirection - targetDirection
		rightCost = 4 + targetDirection - currentDirection
	end

	if leftCost > rightCost then
		for i = 1, rightCost do
			left(rightCost)
		end
	else
		for i = 1, leftCost do
			left(leftCost)
		end
	end
end

local function up(amount)
	if amount == nil then amount = 1 end
	for i = 1, amount, 1 do
		while not turtle.up() do --Player might be in the way.. or another turtle
		os.sleep(.3)
		end
		modCoordinates(directionMod[UP])
	end
end

local function down(amount)
	if amount == nil then amount = 1 end
	for i = 1, amount, 1 do
		while not turtle.down() do --Player might be in the way.. or another turtle
		os.sleep(.3)
		end
		modCoordinates(directionMod[DOWN])
	end
end

local function forward(amount)
	if amount == nil then amount = 1 end
	for i = 1, amount, 1 do
		while not turtle.moveforward() do --Player might be in the way.. or another turtle
		os.sleep(.3)
		end
		modCoordinates(directionMod[curDir])
	end
end

local function back()
end

function movetoX(x, aggressive)

	if relPos.x < x then
		turnToFace(EAST)
	else
		turnToFace(WEST)
	end
	while (relPos.x ~= x) do
		forward()
	end
end

function movetoZ(z, aggressive)
	if relPos.z < z then
		turnToFace(SOUTH)
	else
		turnToFace(NORTH)
	end
	while (relPos.z ~= z) do
		forward()
	end
end

function movetoY(y, aggressive)

	while (relPos.y ~= y) do
		if relPos.y < y then
			up(1)
		else
			down(1)
		end
	end
end

local function moveTo(vec)
	local shouldDig = false
	if (relPos.y > vec.y) then
		movetoY(vec.y)
		movetoZ(vec.z)
		movetoX(vec.x)
	else
		movetoX(vec.x)
		movetoZ(vec.z)
		movetoY(vec.y)
	end
end

local function loadConfig()
	itemBlackList = {}
	--create file if it doesn't exist
	if not fs.exists(blackListFile) then
		local f = fs.open(blackListFile, "w")
		f.write("minecraft:stone")
		f.close()
	end
	local f = fs.open(blackListFile, "r")
	local blacklistEntry = f.readLine()
	while blacklistEntry do
		blacklistEntry = f.readLine()
		itemBlackList[blacklistEntry] = true
	end
	fileList.close()
end

local function loadProgress()
	if fs.exists(currentLocationFile) then
		fh = fs.open(currentLocationFile, "r")
		local loc = textutils.unserialize(fh.readLine())
		fh.close()
		if #loc == 4 then
			relPos.x, relPos.y, relPos.z, curDir = tonumber(loc[1]), tonumber(loc[2]), tonumber(loc[3]), tonumber(loc[4])
		end
	end

	if fs.exists(lastMinedPosition) then
		fh = fs.open(lastMinedPosition, "r")
		local loc = textutils.unserialize(fh.readLine())
		fh.close()
		if #loc >= 3 then
			lastminedPos.x, lastminedPos.y, lastminedPos.z = tonumber(loc[1]), tonumber(loc[2]), tonumber(loc[3]), tonumber(loc[4])
		end
	end
end

function canCollectItems()
	for index = 1, 15 do
		if turtle.getItemCount(index) == 0 then
			return true
		end
	end
	return false
end

loadProgress()
moveTo(HOME)
while canCollectItems() do --mining logic goes here
end
moveTo(HOME)