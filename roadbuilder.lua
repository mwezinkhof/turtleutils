-- Inventory looks like this
---- [ 1, 2, 3, 4]
---- [ 5, 6, 7, 8]
---- [ 9,10,11,12]
---- [13,14,15,16]
-- slots 1 to 8 Slabs
-- slots 9 to 13 half slabs
-- slots 14 to 16 filler blocks; for filling the ground
local tArgs = { ... }
local distance = tonumber(tArgs[1])
distance = distance or 1
if (distance ~= nil and distance > 1 == false) then
	print("Usage: road <length>")
	error()
end
local stairSlotStart = 1
local stairSlotStop = 8
local halfslabStart = stairSlotStop+1
local halfslabStop = stairSlotStop+4
local fillerStart = halfslabStop+1
local fillerStop = 16

local curStairSlot = stairSlotStart
local curHalfslabSlot = halfslabStart
local curFillerSlot = fillerStart

function move(amount)
	amount = amount or 1
	for i=1,amount do
		digUp()
		digforward()
		turtle.forward()
	end
end

function turnaround()
	turtle.turnRight()
	turtle.turnRight()
end

function placefront(slot)
	turtle.select(slot)
	turtle.place()
end

function placeDown(slot)
	turtle.select(slot)
	if turtle.detectDown() then
		turtle.digDown()
	end
	turtle.placeDown()
end

function digforward()
	while turtle.detect() do
		turtle.dig()
		sleep(0.5)
	end
end

function digUp() --same story
while turtle.detectUp() do
	turtle.digUp()
	sleep(0.5)
end
end

function digDown()
	if turtle.detectDown() then
		turtle.digDown()
	end
end

function checkHalfslab()
	if turtle.getItemCount(curHalfslabSlot) < 2 then
		curHalfslabSlot = curHalfslabSlot + 1
	end
	if curHalfslabSlot > halfslabStop then
		print("Out of Halfslabs")
		error()
	end
end

function checkStairs()
	if turtle.getItemCount(curStairSlot) < 2 then
		curStairSlot = curStairSlot + 1
	end
	if curStairSlot > stairSlotStop then
		print("Out of Stairs")
		error()
	end
end
function checkFiller()
	for i=fillerStart,fillerStop do
		if turtle.getItemCount(i) > 2 then
			curFillerSlot = i
			return
		end
	end
	print("Out of filling materials")
	error()
end
function straightRoad(dist)
	for i = 1, distance do
		digUp()
		digDown()
		turtle.down()
		turtle.turnLeft()
		move(1)
		digforward()
		digUp()
		checkFiller()
		placefront(curFillerSlot)
		turtle.back()
		checkStairs()
		placefront(curStairSlot)
		turnaround()
		move(1)
		digforward()
		digUp()
		checkFiller()
		placefront(curFillerSlot)
		turtle.back()
		checkStairs()
		placefront(curStairSlot)
		turtle.turnLeft()
		turtle.up()
		checkHalfslab()
		placeDown(curHalfslabSlot)
		move()
	end
end

straightRoad(distance)

